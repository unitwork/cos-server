#ifndef __COS_SERVER_DB_MANAGER_H__
#define __COS_SERVER_DB_MANAGER_H__
#include <string>
#include <rapidjson/document.h>

#include "conn-pool/ConnPool.h"
#include "MapAlgor.h"
#include "PubInfo.h"
#include "Random.h"
#include "UserInfo.h"
#include "httpclient.h"

#undef MAX
#undef MIN
#include "CosApi.h"

class CCosDb
{
public:
    typedef std::map<std::string, std::string > OrderArgsMap;
	CCosDb();
	~CCosDb();
	static CCosDb* instance();
public:
    bool GetInfoQRCode(rapidjson::Document &document, const std::string &appId, const std::string &pubInfoId, std::string &infoQRCode);
private:
    bool SynInfoQRCode(rapidjson::Document &document, const std::string &appId, const std::string &pubInfoId);
    bool GetAccessToken(rapidjson::Document &document, const std::string &appId, std::string &accessToken);
    bool UrlToInfoQRCode(rapidjson::Document &document, const std::string &appId, const std::string &pubInfoId, std::string &infoQRCode);
    bool InfoQRCodeToRedis(const std::string &pubInfoId, const std::string &infoQRCode);
    bool RedisToInfoQRCode(const std::string &pubInfoId, std::string &infoQRCode);
private:
    qcloud_cos::CosConfig m_cosCfg;
    qcloud_cos::CosAPI m_cos;
    std::string m_qrcodeAurl;
    std::string m_infoPage;
	CSqlConnPool &m_sqlConnPool;
    CRedisConnPool &m_redisConnPool;
    CHttpClient m_httpClient;
	static	CCosDb* _instance;
};
#endif


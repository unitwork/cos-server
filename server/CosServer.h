#ifndef __COS_SERVER_H__
#define __COS_SERVER_H__
#include <string>
#include <map>
#include <boost/thread/mutex.hpp> 
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <rapidjson/document.h>

#include "CosDb.h"

class CCosServer
{
public:
	CCosServer();
	~CCosServer();
	static CCosServer* instance();
private:
    void GetInfoQRCode(rapidjson::Document &document, rapidjson::Value &returnValue);
private:
	static	CCosServer* _instance;	
    typedef boost::function<void(CCosServer*, const std::string&, rapidjson::Value &)> Method;
    typedef std::map<std::string, Method> MethodMap;
    MethodMap m_methodMap;
};

#endif

